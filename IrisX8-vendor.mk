PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/Lava/IrisX8/proprietary/bin,system/bin)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/Lava/IrisX8/proprietary/etc,system/etc)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/Lava/IrisX8/proprietary/lib,system/lib)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/Lava/IrisX8/proprietary/xbin,system/xbin)

PRODUCT_COPY_FILES += $(call find-copy-subdir-files,*,vendor/Lava/IrisX8/proprietary/vendor,system/vendor)
